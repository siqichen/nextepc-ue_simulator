
#define TRACE_MODULE _s1ap_build
#include "core_debug.h"
#include "core_pkbuf.h"
#include "core_lib.h"
#include <mongoc.h>

#include <sys/signal.h>

#include "common/context.h"
#include "mme/mme_context.h"
#include "mme/s1ap_build.h"
#include "s1ap/s1ap_message.h"
#include "gtp/gtp_message.h"

#include "testutil.h"
#include "testpacket.h"

#include <time.h>

// ue-simulator part - milenage algorithm

#include "core_debug.h"
#include "core_aes.h"
#include "core_sha2.h"

#include "core.h"

void milenage_generate(const c_uint8_t *opc, const c_uint8_t *amf,
    const c_uint8_t *k, const c_uint8_t *sqn, const c_uint8_t *_rand,
    c_uint8_t *autn, c_uint8_t *ik, c_uint8_t *ck, c_uint8_t *ak,
    c_uint8_t *res, size_t *res_len);
int milenage_auts(const c_uint8_t *opc, const c_uint8_t *k,
    const c_uint8_t *_rand, const c_uint8_t *auts, c_uint8_t *sqn);
int gsm_milenage(const c_uint8_t *opc, const c_uint8_t *k,
    const c_uint8_t *_rand, c_uint8_t *sres, c_uint8_t *kc);
int milenage_check(const c_uint8_t *opc, const c_uint8_t *k,
    const c_uint8_t *sqn, const c_uint8_t *_rand, const c_uint8_t *autn,
    c_uint8_t *ik, c_uint8_t *ck, c_uint8_t *res, size_t *res_len,
    c_uint8_t *auts);
int milenage_f1(const c_uint8_t *opc, const c_uint8_t *k,
    const c_uint8_t *_rand, const c_uint8_t *sqn, const c_uint8_t *amf,
    c_uint8_t *mac_a, c_uint8_t *mac_s);
int milenage_f2345(const c_uint8_t *opc, const c_uint8_t *k,
    const c_uint8_t *_rand, c_uint8_t *res, c_uint8_t *ck, c_uint8_t *ik,
    c_uint8_t *ak, c_uint8_t *akstar);

void milenage_opc(const c_uint8_t *k, const c_uint8_t *op,  c_uint8_t *opc);

void hsignal(int i) {
	printf("Signal %i\n",i);
	pthread_exit(0);
}

#define os_memcpy memcpy
#define os_memcmp memcmp
#define os_memcmp_const memcmp

int aes_128_encrypt_block(const c_uint8_t *key,
    const c_uint8_t *in, c_uint8_t *out)
{

    const int key_bits = 128;
    unsigned int rk[RKLENGTH(128)];
    int nrounds;

    nrounds = aes_setup_enc(rk, key, key_bits);
    aes_encrypt(rk, nrounds, in, out);

    return 0;
}

/**
 * milenage_f1 - Milenage f1 and f1* algorithms
 * @opc: OPc = 128-bit value derived from OP and K
 * @k: K = 128-bit subscriber key
 * @_rand: RAND = 128-bit random challenge
 * @sqn: SQN = 48-bit sequence number
 * @amf: AMF = 16-bit authentication management field
 * @mac_a: Buffer for MAC-A = 64-bit network authentication code, or %NULL
 * @mac_s: Buffer for MAC-S = 64-bit resync authentication code, or %NULL
 * Returns: 0 on success, -1 on failure
 */
int milenage_f1(const c_uint8_t *opc, const c_uint8_t *k, 
    const c_uint8_t *_rand, const c_uint8_t *sqn, 
    const c_uint8_t *amf, c_uint8_t *mac_a, c_uint8_t *mac_s)
{
	c_uint8_t tmp1[16], tmp2[16], tmp3[16];
	int i;

	/* tmp1 = TEMP = E_K(RAND XOR OP_C) */
	for (i = 0; i < 16; i++)
		tmp1[i] = _rand[i] ^ opc[i];
	if (aes_128_encrypt_block(k, tmp1, tmp1))
		return -1;

	/* tmp2 = IN1 = SQN || AMF || SQN || AMF */
	os_memcpy(tmp2, sqn, 6);
	os_memcpy(tmp2 + 6, amf, 2);
	os_memcpy(tmp2 + 8, tmp2, 8);

	/* OUT1 = E_K(TEMP XOR rot(IN1 XOR OP_C, r1) XOR c1) XOR OP_C */

	/* rotate (tmp2 XOR OP_C) by r1 (= 0x40 = 8 bytes) */
	for (i = 0; i < 16; i++)
		tmp3[(i + 8) % 16] = tmp2[i] ^ opc[i];
	/* XOR with TEMP = E_K(RAND XOR OP_C) */
	for (i = 0; i < 16; i++)
		tmp3[i] ^= tmp1[i];
	/* XOR with c1 (= ..00, i.e., NOP) */

	/* f1 || f1* = E_K(tmp3) XOR OP_c */
	if (aes_128_encrypt_block(k, tmp3, tmp1))
		return -1;
	for (i = 0; i < 16; i++)
		tmp1[i] ^= opc[i];
	if (mac_a)
		os_memcpy(mac_a, tmp1, 8); /* f1 */
	if (mac_s)
		os_memcpy(mac_s, tmp1 + 8, 8); /* f1* */
	return 0;
}


/**
 * milenage_f2345 - Milenage f2, f3, f4, f5, f5* algorithms
 * @opc: OPc = 128-bit value derived from OP and K
 * @k: K = 128-bit subscriber key
 * @_rand: RAND = 128-bit random challenge
 * @res: Buffer for RES = 64-bit signed response (f2), or %NULL
 * @ck: Buffer for CK = 128-bit confidentiality key (f3), or %NULL
 * @ik: Buffer for IK = 128-bit integrity key (f4), or %NULL
 * @ak: Buffer for AK = 48-bit anonymity key (f5), or %NULL
 * @akstar: Buffer for AK = 48-bit anonymity key (f5*), or %NULL
 * Returns: 0 on success, -1 on failure
 */
int milenage_f2345(const c_uint8_t *opc, const c_uint8_t *k, 
    const c_uint8_t *_rand, c_uint8_t *res, c_uint8_t *ck, 
    c_uint8_t *ik, c_uint8_t *ak, c_uint8_t *akstar)
{
	c_uint8_t tmp1[16], tmp2[16], tmp3[16];
	int i;

	/* tmp2 = TEMP = E_K(RAND XOR OP_C) */
	for (i = 0; i < 16; i++)
		tmp1[i] = _rand[i] ^ opc[i];
	if (aes_128_encrypt_block(k, tmp1, tmp2))
		return -1;

	/* OUT2 = E_K(rot(TEMP XOR OP_C, r2) XOR c2) XOR OP_C */
	/* OUT3 = E_K(rot(TEMP XOR OP_C, r3) XOR c3) XOR OP_C */
	/* OUT4 = E_K(rot(TEMP XOR OP_C, r4) XOR c4) XOR OP_C */
	/* OUT5 = E_K(rot(TEMP XOR OP_C, r5) XOR c5) XOR OP_C */

	/* f2 and f5 */
	/* rotate by r2 (= 0, i.e., NOP) */
	for (i = 0; i < 16; i++)
		tmp1[i] = tmp2[i] ^ opc[i];
	tmp1[15] ^= 1; /* XOR c2 (= ..01) */
	/* f5 || f2 = E_K(tmp1) XOR OP_c */
	if (aes_128_encrypt_block(k, tmp1, tmp3))
		return -1;
	for (i = 0; i < 16; i++)
		tmp3[i] ^= opc[i];
	if (res)
		os_memcpy(res, tmp3 + 8, 8); /* f2 */
	if (ak)
		os_memcpy(ak, tmp3, 6); /* f5 */

	/* f3 */
	if (ck) {
		/* rotate by r3 = 0x20 = 4 bytes */
		for (i = 0; i < 16; i++)
			tmp1[(i + 12) % 16] = tmp2[i] ^ opc[i];
		tmp1[15] ^= 2; /* XOR c3 (= ..02) */
		if (aes_128_encrypt_block(k, tmp1, ck))
			return -1;
		for (i = 0; i < 16; i++)
			ck[i] ^= opc[i];
	}

	/* f4 */
	if (ik) {
		/* rotate by r4 = 0x40 = 8 bytes */
		for (i = 0; i < 16; i++)
			tmp1[(i + 8) % 16] = tmp2[i] ^ opc[i];
		tmp1[15] ^= 4; /* XOR c4 (= ..04) */
		if (aes_128_encrypt_block(k, tmp1, ik))
			return -1;
		for (i = 0; i < 16; i++)
			ik[i] ^= opc[i];
	}

	/* f5* */
	if (akstar) {
		/* rotate by r5 = 0x60 = 12 bytes */
		for (i = 0; i < 16; i++)
			tmp1[(i + 4) % 16] = tmp2[i] ^ opc[i];
		tmp1[15] ^= 8; /* XOR c5 (= ..08) */
		if (aes_128_encrypt_block(k, tmp1, tmp1))
			return -1;
		for (i = 0; i < 6; i++)
			akstar[i] = tmp1[i] ^ opc[i];
	}

	return 0;
}


/**
 * milenage_generate - Generate AKA AUTN,IK,CK,RES
 * @opc: OPc = 128-bit operator variant algorithm configuration field (encr.)
 * @amf: AMF = 16-bit authentication management field
 * @k: K = 128-bit subscriber key
 * @sqn: SQN = 48-bit sequence number
 * @_rand: RAND = 128-bit random challenge
 * @autn: Buffer for AUTN = 128-bit authentication token
 * @ik: Buffer for IK = 128-bit integrity key (f4), or %NULL
 * @ck: Buffer for CK = 128-bit confidentiality key (f3), or %NULL
 * @res: Buffer for RES = 64-bit signed response (f2), or %NULL
 * @res_len: Max length for res; set to used length or 0 on failure
 */
void milenage_generate(const c_uint8_t *opc, const c_uint8_t *amf, 
    const c_uint8_t *k, const c_uint8_t *sqn, const c_uint8_t *_rand, 
    c_uint8_t *autn, c_uint8_t *ik, c_uint8_t *ck, c_uint8_t *ak, 
    c_uint8_t *res, size_t *res_len)
{
	int i;
	c_uint8_t mac_a[8];

	if (*res_len < 8) {
		*res_len = 0;
		return;
	}
	if (milenage_f1(opc, k, _rand, sqn, amf, mac_a, NULL) ||
	    milenage_f2345(opc, k, _rand, res, ck, ik, ak, NULL)) {
		*res_len = 0;
		return;
	}
	*res_len = 8;

	/* AUTN = (SQN ^ AK) || AMF || MAC */
	for (i = 0; i < 6; i++)
		autn[i] = sqn[i] ^ ak[i];
	os_memcpy(autn + 6, amf, 2);
	os_memcpy(autn + 8, mac_a, 8);
}

/**
 * milenage_auts - Milenage AUTS validation
 * @opc: OPc = 128-bit operator variant algorithm configuration field (encr.)
 * @k: K = 128-bit subscriber key
 * @_rand: RAND = 128-bit random challenge
 * @auts: AUTS = 112-bit authentication token from client
 * @sqn: Buffer for SQN = 48-bit sequence number
 * Returns: 0 = success (sqn filled), -1 on failure
 */
int milenage_auts(const c_uint8_t *opc, const c_uint8_t *k, 
    const c_uint8_t *_rand, const c_uint8_t *auts, c_uint8_t *sqn)
{
	c_uint8_t amf[2] = { 0x00, 0x00 }; /* TS 33.102 v7.0.0, 6.3.3 */
	c_uint8_t ak[6], mac_s[8];
	int i;

	if (milenage_f2345(opc, k, _rand, NULL, NULL, NULL, NULL, ak))
		return -1;
	for (i = 0; i < 6; i++)
		sqn[i] = auts[i] ^ ak[i];
	if (milenage_f1(opc, k, _rand, sqn, amf, NULL, mac_s) ||
	    os_memcmp_const(mac_s, auts + 6, 8) != 0)
		return -1;
	return 0;
}


/**
 * gsm_milenage - Generate GSM-Milenage (3GPP TS 55.205) authentication triplet
 * @opc: OPc = 128-bit operator variant algorithm configuration field (encr.)
 * @k: K = 128-bit subscriber key
 * @_rand: RAND = 128-bit random challenge
 * @sres: Buffer for SRES = 32-bit SRES
 * @kc: Buffer for Kc = 64-bit Kc
 * Returns: 0 on success, -1 on failure
 */
int gsm_milenage(const c_uint8_t *opc, const c_uint8_t *k, 
    const c_uint8_t *_rand, c_uint8_t *sres, c_uint8_t *kc)
{
	c_uint8_t res[8], ck[16], ik[16];
	int i;

	if (milenage_f2345(opc, k, _rand, res, ck, ik, NULL, NULL))
		return -1;

	for (i = 0; i < 8; i++)
		kc[i] = ck[i] ^ ck[i + 8] ^ ik[i] ^ ik[i + 8];

#ifdef GSM_MILENAGE_ALT_SRES
	os_memcpy(sres, res, 4);
#else /* GSM_MILENAGE_ALT_SRES */
	for (i = 0; i < 4; i++)
		sres[i] = res[i] ^ res[i + 4];
#endif /* GSM_MILENAGE_ALT_SRES */
	return 0;
}


/**
 * milenage_generate - Generate AKA AUTN,IK,CK,RES
 * @opc: OPc = 128-bit operator variant algorithm configuration field (encr.)
 * @k: K = 128-bit subscriber key
 * @sqn: SQN = 48-bit sequence number
 * @_rand: RAND = 128-bit random challenge
 * @autn: AUTN = 128-bit authentication token
 * @ik: Buffer for IK = 128-bit integrity key (f4), or %NULL
 * @ck: Buffer for CK = 128-bit confidentiality key (f3), or %NULL
 * @res: Buffer for RES = 64-bit signed response (f2), or %NULL
 * @res_len: Variable that will be set to RES length
 * @auts: 112-bit buffer for AUTS
 * Returns: 0 on success, -1 on failure, or -2 on synchronization failure
 */
int milenage_check(const c_uint8_t *opc, const c_uint8_t *k, 
    const c_uint8_t *sqn, const c_uint8_t *_rand, const c_uint8_t *autn, 
    c_uint8_t *ik, c_uint8_t *ck, c_uint8_t *res, size_t *res_len,
    c_uint8_t *auts)
{
	int i;
	c_uint8_t mac_a[8], ak[6], rx_sqn[6];
	const c_uint8_t *amf;

    d_trace(1, "Milenage: AUTN\n"); d_trace_hex(1, autn, 16);
    d_trace(1, "Milenage: RAND\n"); d_trace_hex(1, _rand, 16);

	if (milenage_f2345(opc, k, _rand, res, ck, ik, ak, NULL))
		return -1;

	*res_len = 8;
    d_trace(1, "Milenage: RES\n"); d_trace_hex(1, res, *res_len);
    d_trace(1, "Milenage: CK\n"); d_trace_hex(1, ck, 16);
    d_trace(1, "Milenage: IK\n"); d_trace_hex(1, ik, 16);
    d_trace(1, "Milenage: AK\n"); d_trace_hex(1, ak, 6);

	/* AUTN = (SQN ^ AK) || AMF || MAC */
	for (i = 0; i < 6; i++)
		rx_sqn[i] = autn[i] ^ ak[i];
    d_trace(1, "Milenage: SQN\n"); d_trace_hex(1, rx_sqn, 6);

	if (os_memcmp(rx_sqn, sqn, 6) <= 0) {
		c_uint8_t auts_amf[2] = { 0x00, 0x00 }; /* TS 33.102 v7.0.0, 6.3.3 */
		if (milenage_f2345(opc, k, _rand, NULL, NULL, NULL, NULL, ak))
			return -1;
        d_trace(1, "Milenage: AK*\n"); d_trace_hex(1, ak, 6);
		for (i = 0; i < 6; i++)
			auts[i] = sqn[i] ^ ak[i];
		if (milenage_f1(opc, k, _rand, sqn, auts_amf, NULL, auts + 6))
			return -1;
        d_trace(1, "Milenage: AUTS*\n"); d_trace_hex(1, auts, 14);
		return -2;
	}

	amf = autn + 6;
    d_trace(1, "Milenage: AMF\n"); d_trace_hex(1, amf, 2);
	if (milenage_f1(opc, k, _rand, rx_sqn, amf, mac_a, NULL))
		return -1;

    d_trace(1, "Milenage: MAC_A\n"); d_trace_hex(1, mac_a, 8);

	if (os_memcmp_const(mac_a, autn + 8, 8) != 0) {
        d_trace(1, "Milenage: MAC mismatch\n");
        d_trace(1, "Milenage: Received MAC_A\n"); d_trace_hex(1, autn + 8, 8);
		return -1;
	}

	return 0;
}

void milenage_opc(const c_uint8_t *k, const c_uint8_t *op,  c_uint8_t *opc)
{
    int i;

    aes_128_encrypt_block(k,  op, opc);

    for (i = 0; i < 16; i++)
    {
        opc[i] ^= op[i];
    }
}
int calc_from_hex(char *src, int len) {
	int res=0;
	int i=0;
	for (; i<len ; ++i) {
		res = res*16 + src[i];
	}
	return res;
}

// src format : "xx...xx : " then followed by hex part
void turn_str_into_hex(c_uint8_t *dest, char* src) {
        int i,j;
        for (i=0; src[i]; ++i) {
                if ( src[i] == ':' ) break;
        }
        int lastNum = -1;
        int numbase[4] = {-1, '0', 'a'-10, 'A'-10};
        for (j=0; src[i]; ++i) {
                int flag = 0;
                if ( src[i] >= '0' && src[i] <= '9' ) flag = 1;
                if ( src[i] >= 'a' && src[i] <= 'f' ) flag = 2;
                if ( src[i] >= 'A' && src[i] <= 'F' ) flag = 3;

                if ( flag != 0 ) {
                        if ( lastNum != -1 ) {
                                dest[j++] = lastNum*16 + src[i] - numbase[flag];
                                lastNum = -1;
                        }
                        else
                                lastNum = src[i] - numbase[flag];
                }
        }
}

// UE simulator - Kasme part
 #include "core_debug.h"
#include "core_sha2_hmac.h"
#include "3gpp_types.h"

#define FC_VALUE 0x10

void hss_auc_kasme_ue_simulator(const c_uint8_t *ck, const c_uint8_t *ik,
        const c_uint8_t plmn_id[3], const c_uint8_t *sqnxorak, 
        c_uint8_t *kasme)
{
    c_uint8_t s[14];
    c_uint8_t k[32];
    int i;

    memcpy(&k[0], ck, 16);
    memcpy(&k[16], ik, 16);

    s[0] = FC_VALUE;
    memcpy(&s[1], plmn_id, 3);
    s[4] = 0x00;
    s[5] = 0x03;

    for (i = 0; i < 6; i++)
        s[6+i] = sqnxorak[i];
    s[12] = 0x00;
    s[13] = 0x06;

    hmac_sha256(k, 32, s, 14, kasme, 32);
}
#include "core_sha2_hmac.h"

void mme_kdf_nas_ue_simulator(c_uint8_t algorithm_type_distinguishers,
    c_uint8_t algorithm_identity, c_uint8_t *kasme, c_uint8_t *knas)
{
    c_uint8_t s[7];
    c_uint8_t out[32];

    s[0] = 0x15; /* FC Value */

    s[1] = algorithm_type_distinguishers;
    s[2] = 0x00;
    s[3] = 0x01;

    s[4] = algorithm_identity;
    s[5] = 0x00;
    s[6] = 0x01;

    hmac_sha256(kasme, 32, s, 7, out, 32);
    memcpy(knas, out+16, 16);
}

#include <pthread.h>
#include <semaphore.h>

#define THREADNUM 500 
#define CONCURRENT 0 
sock_id enb_sock1, gtpu_sock1;
sock_id enb_sock2, gtpu_sock2;
char ip_epc[16];
char ip_enb[16];
pkbuf_t *recvbuf;
pkbuf_t *recvbuf2;
pkbuf_t *recvbuf_gtpu;
pkbuf_t *recvbuf_thread[THREADNUM];
// number of waiting reading
sem_t occupied_s1ap_read;
sem_t occupied_gtpu_read;
// 0/1 mutex for each UE
sem_t received_sem_ue[THREADNUM];
int ue_teid[THREADNUM];

// declaring mutex
pthread_mutex_t s1ap_send_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t gtpu_send_lock = PTHREAD_MUTEX_INITIALIZER;


void sock_init(char *ip_local1, char *ip_local2) {
    char buf[100];
    status_t rv;
    int i;

    sem_init(&occupied_s1ap_read, 0, 0);
    sem_init(&occupied_gtpu_read, 0, 0);
    for ( i = 0 ; i < THREADNUM ; ++i ) sem_init(&received_sem_ue[i], 0, 0);
    for ( i = 0 ; i < THREADNUM ; ++i ) ue_teid[i] = -1;
    recvbuf2 = pkbuf_alloc(0, MAX_SDU_LEN);

    FILE *confFile = fopen("./test/basic/test.conf","r");
    if ( confFile == NULL ) {
    	printf("ERROR! test.conf can not be found!\n");
	return ;
    }
    while(fscanf(confFile,"%s :",buf) == 1) {
    	if ( strcmp(buf, "epc") == 0 ) {
		fscanf(confFile, "%s", ip_epc);
	}
	else if ( strcmp(buf,"enodeb") == 0 ) {
		fscanf(confFile, "%s", ip_enb);
	}
	else {
		printf("ERROR! wrong configuration file format! reading:[%s]\n",buf);
	}
    }
    fclose(confFile);

    pkbuf_t *sendbuf;
    s1ap_message_t message;

    // 1st eNB attach 
    /* eNB connects to MME */
    rv = tests1ap_enb_connect2(&enb_sock1, ip_epc, ip_local1);
    //rv = tests1ap_connect(&sock, ip_epc);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    /* eNB connects to SGW */
    rv = testgtpu_enb_connect2(&gtpu_sock1, ip_local1);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = CORE_OK;
    if ( rv != CORE_OK )
	    printf("ERROR\n");
    /* Send S1-Setup Reqeust */
    rv = tests1ap_build_setup_req(&sendbuf, S1AP_ENB_ID_PR_macroENB_ID, 0x54f64);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* Receive S1-Setup Response */
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(enb_sock1, recvbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = s1ap_decode_pdu(&message, recvbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    s1ap_free_pdu(&message);
    pkbuf_free(recvbuf);

    /*
    // 2nd eNB attach
    rv = tests1ap_enb_connect2(&enb_sock2, ip_epc, ip_local2);
    rv = testgtpu_enb_connect2(&gtpu_sock2, ip_local2);
    // Send S1-Setup Request
    rv = tests1ap_build_setup_req(&sendbuf, S1AP_ENB_ID_PR_macroENB_ID, 0x54f65);
    rv = tests1ap_enb_send(enb_sock2, sendbuf);
    // Receive S1-Setup Response 
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(enb_sock2, recvbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = s1ap_decode_pdu(&message, recvbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    s1ap_free_pdu(&message);
    pkbuf_free(recvbuf);
    */
}
void sock_close() {
    status_t rv ;
    /* eNB disonncect from MME */
    rv = tests1ap_enb_close(enb_sock1);
    //rv = tests1ap_enb_close(enb_sock2);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    /* eNB disonncect from SGW */
    rv = testgtpu_enb_close(gtpu_sock1);
    //rv = testgtpu_enb_close(gtpu_sock2);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    if ( rv != CORE_OK )
	printf("ERROR\n");
    pkbuf_free(recvbuf2);
}

int all_terminated = 0;
void *thread_gtpu_read(void *arg) {
	gtp_header_t *gtp_h = NULL;
	int rc;
	int i;
	c_uint32_t teid, teid_reversed;

    	//signal(SIGINT, hsignal);
	while(1) {
		sem_wait(&occupied_gtpu_read);
		if ( all_terminated )
			return NULL;

		recvbuf_gtpu = pkbuf_alloc(0, 200 /*enough for ICMP*/);
		rc = 0;
		while(1) {
			rc = core_recv(gtpu_sock1, recvbuf_gtpu->payload, recvbuf_gtpu->len, 0);
			if ( rc == -2 ) continue;
			else if ( rc <= 0 ) {
				if ( errno == EAGAIN) continue;
				break;
			}
			else break;
		}
		recvbuf_gtpu->len = rc;
		gtp_h = (gtp_header_t*) recvbuf_gtpu->payload;
		teid_reversed = gtp_h->teid;
		teid = 0;
		while(teid_reversed) {
			teid = (teid<<8)+(teid_reversed&(0xff));
			teid_reversed>>=8;
		}
		for ( i = 0 ; i < THREADNUM ; ++i )
			if ( ue_teid[i] == teid ) break;
		if ( i == THREADNUM ) {
			fprintf(stderr,"ERROR: no thread found for teid %d\n",teid);
			fprintf(stderr,"len: %d\n",recvbuf_gtpu->len);
			for ( i = 0 ; i < recvbuf_gtpu->len ; ++i )
				fprintf(stderr,"%x ",*(char*)(recvbuf_gtpu->payload+i));
			fprintf(stderr,"\n");
			pkbuf_free(recvbuf_gtpu);
			continue;
		}
		recvbuf_thread[i] = pkbuf_alloc(0, 200);
		memcpy(recvbuf_thread[i]->payload, recvbuf_gtpu->payload, recvbuf_gtpu->len);
		recvbuf_thread[i]->len = recvbuf_gtpu->len;
		pkbuf_free(recvbuf_gtpu);	
		sem_post(&received_sem_ue[i]);
	}
	return NULL; 
}
void *thread_s1ap_read(void *arg) 
{ 
    s1ap_message_t message;
    int i;
    long enb_ue_s1ap_id = 0;
    status_t rv;

    //signal(SIGINT, hsignal);
    while(1) {

	sem_wait(&occupied_s1ap_read);
	if ( all_terminated )
		return NULL;
//printf("[thread s1ap enb reading start]\n");
	
	recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
	rv = tests1ap_enb_read(enb_sock1, recvbuf);
	if ( rv != CORE_OK )
		break;
//printf("[thread s1ap enb reading end]\n");
	memcpy(recvbuf2->payload, recvbuf->payload, recvbuf->len);
 	recvbuf2->len = recvbuf->len;
	//printf("[s1ap] RECV: %d\n",recvbuf2->len);
	//for(i=0;i<recvbuf2->len;++i)
	//	printf("%x ",*(char*)(recvbuf2->payload+i));
	//printf("\n");
	s1ap_decode_pdu(&message,recvbuf2);
	for(i=0;;++i) {
		int flag = 0;
		if (message.present == S1AP_S1AP_PDU_PR_initiatingMessage) {
			switch (message.choice.initiatingMessage->procedureCode) {
				case S1AP_ProcedureCode_id_InitialContextSetup:
					if ( message.choice.initiatingMessage->value.choice.InitialContextSetupRequest.protocolIEs.list.array[i]->value.present == S1AP_InitialContextSetupRequestIEs__value_PR_ENB_UE_S1AP_ID ) {
						flag = 1;
						enb_ue_s1ap_id = message.choice.initiatingMessage->value.choice.InitialContextSetupRequest.protocolIEs.list.array[i]->value.choice.ENB_UE_S1AP_ID;
					}
					break;
				case S1AP_ProcedureCode_id_downlinkNASTransport:
					if ( message.choice.initiatingMessage->value.choice.DownlinkNASTransport.protocolIEs.list.array[i]->value.present == S1AP_DownlinkNASTransport_IEs__value_PR_ENB_UE_S1AP_ID ) {
						flag = 1;
						enb_ue_s1ap_id = message.choice.initiatingMessage->value.choice.DownlinkNASTransport.protocolIEs.list.array[i]->value.choice.ENB_UE_S1AP_ID;
					}
					break;
				case S1AP_ProcedureCode_id_UEContextRelease:
					if ( message.choice.initiatingMessage->value.choice.UEContextReleaseCommand.protocolIEs.list.array[i]->value.present == S1AP_UEContextReleaseCommand_IEs__value_PR_UE_S1AP_IDs ) {
						flag = 1 ;
						enb_ue_s1ap_id = message.choice.initiatingMessage->value.choice.UEContextReleaseCommand.protocolIEs.list.array[i]->value.choice.UE_S1AP_IDs.choice.uE_S1AP_ID_pair->eNB_UE_S1AP_ID;
					}
					break;
				default:
					printf("NOT HANDLE [initiaing message procedure code] %ld\n",message.choice.initiatingMessage->procedureCode);
			}
		}
		else if ( message.present == S1AP_S1AP_PDU_PR_successfulOutcome) {
			printf("NOT HANDLE [successful outcome]\n");
			break;	
		}
		else if ( message.present == S1AP_S1AP_PDU_PR_unsuccessfulOutcome) {
			printf("NOT HANDLE [unsuccessful outcome]\n");
			break;
		}
		else {
			printf("UNKNOW TYPE[S1AP MSG] %d\n",message.present);
			break;
		}

		if ( flag )
			break;
	}
	recvbuf_thread[enb_ue_s1ap_id-1] = pkbuf_alloc(0, MAX_SDU_LEN);
	memcpy(recvbuf_thread[enb_ue_s1ap_id-1]->payload, recvbuf->payload, recvbuf->len);
	recvbuf_thread[enb_ue_s1ap_id-1]->len = recvbuf->len;
	s1ap_free_pdu(&message);
	pkbuf_free(recvbuf);	
//printf("[thread signal] enb_ue_s1ap_id: %ld\n",enb_ue_s1ap_id);

        sem_post(&received_sem_ue[enb_ue_s1ap_id-1]);
    }
    return NULL; 
} 

struct UE_LOG {
    	clock_t init_ue_msg_time; 
	clock_t auth_request_time;
    	clock_t auth_response_time ;
	clock_t security_mode_command_time ;
	clock_t security_mode_complete_time ;
	clock_t ESM_info_request_time ;
	clock_t ESM_info_response_time ;
	clock_t UE_info_time ;
	clock_t initial_context_setup_receive_time;
	clock_t initial_context_setup_send_time ;
    	clock_t activate_EPS_time ;
    	clock_t receive_EMM_time ; 
    	clock_t ping_time ;
	clock_t detach_request_time ;
	clock_t UE_release_command_time ;
	clock_t UE_release_complete_time ;
	clock_t last_update ;

}ue_log[THREADNUM];

#define log_update(x,y) { x = clock() - y; y = clock(); }
#define log_reset_time(x) { x = clock(); }
/*
void log_update(clock_t *p, clock_t *last_update) {
	*p = clock() - *last_update;
	*last_update = clock();
}
*/

/**************************************************************
 * eNB : MACRO
 * UE : IMSI 
 * Protocol Configuration Options in ESM information response */
static void attach_test1(abts_case *tc, void *data)
{
    int ue_id = ((int*)data)[0]; // 
    if ( ue_id < 0 || ue_id > 10000 ) { printf("returned!\n"); return ; }


    struct UE_LOG *log = &ue_log[ue_id];
    log_reset_time(log->last_update);

    status_t rv;
    pkbuf_t *sendbuf;
    s1ap_message_t message;
    int i;
    int msgindex = 0;
    int temp_id;
    enb_ue_t *enb_ue = NULL;
    mme_ue_t *mme_ue = NULL;
    c_uint32_t m_tmsi = 0;
    char ip_local1[16] = "192.168.1.2";
    char ip_local2[16] = "192.168.1.9";

    c_uint8_t tmp[MAX_SDU_LEN];

    // UE_FILE for plot
    char UE_FILE_NAME[75] = {"/home/internetlab/UEplot/"};
    int end_p = strlen(UE_FILE_NAME);
    int end_pointer = end_p;
    temp_id = ue_id;
    do{
	UE_FILE_NAME[end_pointer++] = '0'+temp_id % 10;
	temp_id /= 10;
    }while(temp_id);
    UE_FILE_NAME[end_pointer--] = '\0';
    for ( i = 0 ; end_p+i < end_pointer-i ; ++i ) {
	    char tempC = UE_FILE_NAME[end_p+i];
	    UE_FILE_NAME[end_p+i] = UE_FILE_NAME[end_pointer-i];
	    UE_FILE_NAME[end_pointer-i] = tempC;
    }

    /*
    char *_authentication_request = 
        "000b403b00000300 000005c00100009d 000800020001001a 0025240752002008"
        "0c3818183b522614 162c07601d0d10f1 1b89a2a8de8000ad 0ccf7f55e8b20d";
    char *_security_mode_command = 
        "000b402700000300 000005c00100009d 000800020001001a 00111037f933b5d5"
        "00075d010005e060 c04070";
    char *_esm_information_request =
        "000b402000000300 000005c00100009d 000800020001001a 000a092779012320"
        "010221d9";
    char *_initial_context_setup_request = 
        "00090080d8000006 00000005c0010000 9d00080002000100 42000a183e800000"
        "603e800000001800 8086000034008080 450009200f807f00 0002000000017127"
        "4db5d98302074202 49064000f1105ba0 00485221c1010909 08696e7465726e65"
        "7405012d2d00025e 06fefeeeee030327 2980c22304030000 0480211002000010"
        "8106080808088306 08080404000d0408 080808000d040808 0404500bf600f110"
        "0002010000000153 12172c5949640125 006b000518000c00 00004900203311c6"
        "03c6a6d67f695e5a c02bb75b381b693c 3893a6d932fd9182 3544e3e79b";
    char *_emm_information = 
        "000b403b00000300 000005c00100009d 000800020001001a 002524271f9b491e"
        "030761430f10004e 0065007800740045 0050004347812072 11240563490100";
*/
    mongoc_collection_t *collection = NULL;
    bson_t *doc = NULL;
    c_int64_t count = 0;
    bson_error_t error;

    char json1[] =   "{"
        "\"_id\" : { \"$oid\" : \"597223158b8861d7605378c6\" }, " 
	"\"imsi\" : \"";
    char json2[16] = {"00101012345"} ;
    int loop_index = 14;
    temp_id = ue_id + 23456819;
    while(temp_id) {
	    json2[loop_index--] = temp_id%10 + '0';
	    temp_id /= 10;
    }
    char json3[] = "\", "
        "\"pdn\" : ["
          "{"
            "\"apn\" : \"internet\", "
            "\"_id\" : { \"$oid\" : \"597223158b8861d7605378c7\" }, "
            "\"ambr\" : {"
              "\"uplink\" : { \"$numberLong\" : \"1024000\" }, "
              "\"downlink\" : { \"$numberLong\" : \"1024000\" } "
            "},"
            "\"qos\" : { "
              "\"qci\" : 9, "
              "\"arp\" : { "
                "\"priority_level\" : 8,"
                "\"pre_emption_vulnerability\" : 1, "
                "\"pre_emption_capability\" : 1"
              "} "
            "}, "
            "\"type\" : 2"
          "}"
        "],"
        "\"ambr\" : { "
          "\"uplink\" : { \"$numberLong\" : \"1024000\" }, "
          "\"downlink\" : { \"$numberLong\" : \"1024000\" } "
        "},"
        "\"subscribed_rau_tau_timer\" : 12,"
        "\"network_access_mode\" : 2, "
        "\"subscriber_status\" : 0, "
        "\"access_restriction_data\" : 32, "
        "\"security\" : { ";
    char k_str[] = "\"k\" : \"465B5CE8 B199B49F AA5F0A2E E238A6BC\", ";
    char opc_str[] = "\"opc\" : \"E8ED289D EBA952E4 283B54E8 8E6183CA\", ";
    char amf_str[] = "\"amf\" : \"8000\", ";
    char sqn_str[] = "\"sqn\" : { \"$numberLong\" : \"64\" }, ";
    char rand_str[] = "\"rand\" : \"20080C38 18183B52 2614162C 07601D0D\" ";
    char json4[] =
        "}, "
        "\"__v\" : 0 "
      "}";
    char *json;
    json = malloc( (1 + strlen(json1) + strlen(json2) + strlen(json3) + strlen(k_str) + strlen(opc_str) + strlen(amf_str) + strlen(sqn_str) + strlen(rand_str) + strlen(json4)) *sizeof(char));
    strcpy(json,json1);
    strcat(json,json2);
    strcat(json,json3);
    strcat(json,k_str);
    strcat(json,opc_str);
    strcat(json,amf_str);
    strcat(json,sqn_str);
    strcat(json,rand_str);
    strcat(json,json4);

    //core_sleep(time_from_msec(300));


/*
    collection = mongoc_client_get_collection(
        context_self()->db_client,
        context_self()->db_name, "subscribers");
    printf("DATABASE: name[%s] %s\n",context_self()->db_name,"subscribers");
    ABTS_PTR_NOTNULL(tc, collection);
*/
    /********** Insert Subscriber in Database */
    /*
    doc = bson_new_from_json((const uint8_t *)json, -1, &error);;
    printf("INSERT Subscriber\n");
    printf("%s\n",json);
    ABTS_PTR_NOTNULL(tc, doc);

    int insert_res = mongoc_collection_insert(collection, MONGOC_INSERT_NONE, doc, NULL, &error);
    ABTS_TRUE(tc, insert_res);
    if(!insert_res) {
    	fprintf (stderr, "ERROR: %d.%d: %s\n", error.domain, error.code, error.message);
	    doc = BCON_NEW("imsi", BCON_UTF8(json2));
	    ABTS_PTR_NOTNULL(tc, doc);
	    ABTS_TRUE(tc, mongoc_collection_remove(collection,
				    MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, &error))
		    bson_destroy(doc);

	    mongoc_collection_destroy(collection);
	free(json);
	return ;
    }


    bson_destroy(doc);

    doc = BCON_NEW("imsi", BCON_UTF8(json2));
    ABTS_PTR_NOTNULL(tc, doc);
    do
    {
        count = mongoc_collection_count (
            collection, MONGOC_QUERY_NONE, doc, 0, 0, NULL, &error);
    } while (count == 0);
    bson_destroy(doc);
*/
    free(json);
    /***********************************************************************
     * Attach Request : Known IMSI, Integrity Protected, No Security Context
     * Send Initial-UE Message + Attach Request + PDN Connectivity        */
    //printf("[ATTACH REQUEST]\n");
    //core_sleep(time_from_msec(300));

    long enb_ue_s1ap_id = ue_id + 1;

    //mme_self()->mme_ue_s1ap_id = 16777372+ue_id; // was using to manually set up mme_ue_id
    S1AP_MME_UE_S1AP_ID_t mme_ue_s1ap_id = 0 ; // will be assigned from DownLink Authentication Request

    //rv = tests1ap_build_initial_ue_msg(&sendbuf, msgindex);
    pthread_mutex_lock(&s1ap_send_lock);
    log_reset_time(log->last_update);
    rv = s1ap_build_initial_ue_message(&sendbuf, enb_ue_s1ap_id, json2 /*001010123456819+ue_id*/); // siqi's test function
    printf("[SEND INITIAL UE MESSAGE] %ld\n",enb_ue_s1ap_id);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    pthread_mutex_unlock(&s1ap_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    // UE plot update
    FILE *UE_FILE;
    UE_FILE = fopen(UE_FILE_NAME, "w");
    fclose(UE_FILE);
   
    log_update( log->init_ue_msg_time, log->last_update ); 

    /* Receive Authentication Request */
    /*
     * Current simulator doesn't check the AUTN from EnodeB
     * It assumes EnodeB is always trusted
     *
     * */
    printf("[RECEIVE AUTHENTICATION REQUEST] %ld\n",enb_ue_s1ap_id);
    //recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    //rv = tests1ap_enb_read(sock, recvbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    //ABTS_TRUE(tc, memcmp(recvbuf->payload, 
    //    CORE_HEX(_authentication_request, strlen(_authentication_request), tmp),
    //    recvbuf->len) == 0);
   
    //printf("[S1AP DECODE]\n") ;

    sem_post(&occupied_s1ap_read);
    sem_wait(&received_sem_ue[enb_ue_s1ap_id-1]);

    log_update(log->auth_request_time, log->last_update);

    s1ap_message_t auth_req_message;
    s1ap_decode_pdu(&auth_req_message, recvbuf_thread[enb_ue_s1ap_id-1]);
    struct S1AP_DownlinkNASTransport_IEs *ie;
    // get item 0 : mme-ue-s1ap-id
    if (auth_req_message.choice.initiatingMessage->value.choice.DownlinkNASTransport.protocolIEs.list.array[0]->value.present != S1AP_DownlinkNASTransport_IEs__value_PR_MME_UE_S1AP_ID) // assumption: always item 0(not 1 or 2)
	    fprintf(stderr,"Anthentication request: Item 0 is not mme-ue-s1ap-id\n");
    mme_ue_s1ap_id = auth_req_message.choice.initiatingMessage->value.choice.DownlinkNASTransport.protocolIEs.list.array[0]->value.choice.MME_UE_S1AP_ID;

    //printf("[thread info] mme-ue: %ld, enb-ue: %ld\n", mme_ue_s1ap_id, enb_ue_s1ap_id);

    ie = auth_req_message.choice.initiatingMessage->value.choice.DownlinkNASTransport.protocolIEs.list.array[2];
    nas_message_t auth_req;
    pkbuf_t *nas_pdu_buf;
    nas_pdu_buf = pkbuf_alloc(0, 4000);
    memcpy( nas_pdu_buf->payload, ie->value.choice.NAS_PDU.buf, ie->value.choice.NAS_PDU.size);
    nas_pdu_buf->tot_len = nas_pdu_buf->len = ie->value.choice.NAS_PDU.size;

    nas_emm_decode(&auth_req,nas_pdu_buf); // get nas_pdu item
    pkbuf_free(nas_pdu_buf);

    //printf("[AUTHENTICATION] RAND:\n");
    sqn_str[0] = ':';
    for (i=0;i<6;++i)
    	sprintf(sqn_str+2*i+1, "%02x",auth_req.emm.authentication_request.authentication_parameter_autn.autn[i]);
    s1ap_free_pdu(&auth_req_message);
    pkbuf_free(recvbuf_thread[enb_ue_s1ap_id-1]);
//return ;

    c_uint8_t opc[16], amf[16], k[16], sqn[16], _rand[16];
    turn_str_into_hex(opc, opc_str);
    turn_str_into_hex(amf, amf_str);
    turn_str_into_hex(k, k_str);
    turn_str_into_hex(sqn, sqn_str);
    turn_str_into_hex(_rand, rand_str);
    c_uint8_t autn[16], ik[16], ck[16], ak[16];
    c_uint8_t res[8];
    size_t length = 8; // if less than 8, generate will return failure
    milenage_generate( opc, amf, k, sqn, _rand, autn, ik, ck, ak, res, &length);

    /* Send Authentication Response */
    printf("[SEND AUTHENTICATION RESPONSE] %ld\n", enb_ue_s1ap_id);
    //rv = tests1ap_build_authentication_response(&sendbuf, msgindex);
    pthread_mutex_lock(&s1ap_send_lock);
    log_reset_time( log->last_update);
    rv = s1ap_build_authentication_response(&sendbuf, res, length, enb_ue_s1ap_id, mme_ue_s1ap_id); // siqi's function
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    pthread_mutex_unlock(&s1ap_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    log_update( log->auth_response_time, log->last_update);
//return ;
    /* Receive Security mode Command */
    printf("[RECEIVE SECURITY MODE COMMAND] %ld\n", enb_ue_s1ap_id);
    //recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    //rv = tests1ap_enb_read(sock, recvbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    //ABTS_TRUE(tc, memcmp(recvbuf->payload,
    //    CORE_HEX(_security_mode_command, strlen(_security_mode_command), tmp),
    //    recvbuf->len) == 0);
    sem_post(&occupied_s1ap_read);
    sem_wait(&received_sem_ue[enb_ue_s1ap_id-1]);
    pkbuf_free(recvbuf_thread[enb_ue_s1ap_id-1]);
    log_update( log->security_mode_command_time, log->last_update);

// return ;
    /* Current simulator didn't check the choice of EIA and EEA
     * It assumes always using the EIA1 for integrity check and EEA0 (no cipher)*/

    // generate kasme 
    c_uint8_t plmn_id[3] = {0x00, 0xf1, 0x10}; // TODO: it should be generated for each UE, rather than be constant now
    c_uint8_t kasme[32]; // should be 32-bit

    hss_auc_kasme_ue_simulator(ck, ik, plmn_id, sqn, kasme);
    // generate knas_int, knas_enc 
    c_uint8_t knas_int[16], knas_enc[16];

    mme_kdf_nas_ue_simulator(0x2,/*MME_KDF_NAS_INT_ALG*/ 0x1,/*mme_ue->selected_int_algorithm,*/
            kasme, knas_int);
    mme_kdf_nas_ue_simulator(0x1,/*MME_KDF_NAS_ENC_ALG*/ 0x0,/*mme_ue->selected_enc_algorithm,*/
            kasme, knas_enc);
    /* Send Security mode Complete */
    printf("[SEND SECURITY MODE COMPLETE] %ld\n", enb_ue_s1ap_id);
    //rv = tests1ap_build_security_mode_complete(&sendbuf, msgindex);
    pthread_mutex_lock(&s1ap_send_lock);
    log_reset_time(log->last_update);
    // timer start
    rv = s1ap_build_security_mode_complete(&sendbuf, mme_ue_s1ap_id, enb_ue_s1ap_id, knas_int); // siqi's function
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    pthread_mutex_unlock(&s1ap_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    log_update(log->security_mode_complete_time, log->last_update);

// return ;
    /* Receive ESM Information Request */
    printf("[RECEIVE ESM INFORMATION REQUEST] %ld\n", enb_ue_s1ap_id);
    //recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    //rv = tests1ap_enb_read(sock, recvbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    //ABTS_TRUE(tc, memcmp(recvbuf->payload, 
    //    CORE_HEX(_esm_information_request, strlen(_security_mode_command), tmp),
    //    recvbuf->len) == 0);
    sem_post(&occupied_s1ap_read);
    sem_wait(&received_sem_ue[enb_ue_s1ap_id-1]);

    pkbuf_free(recvbuf_thread[enb_ue_s1ap_id-1]);
    log_update( log->ESM_info_request_time, log->last_update);
// return ;

    /* Send ESM Information Response */
    //printf("[SEND ESM INFORMATION RESPONSE] %ld\n", enb_ue_s1ap_id);
    pthread_mutex_lock(&s1ap_send_lock);
    log_reset_time( log->last_update);
    rv = s1ap_build_esm_information_response(&sendbuf, mme_ue_s1ap_id, enb_ue_s1ap_id, knas_int); // siqi's function
    //rv = tests1ap_build_esm_information_response(&sendbuf, msgindex);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    pthread_mutex_unlock(&s1ap_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    log_update( log->ESM_info_response_time, log->last_update);

    /* Receive Initial Context Setup Request + 
     * Attach Accept + 
     * Activate Default Bearer Context Request */
    //printf("[RECEIVE INITIAL CONTEXT SETUP] %ld\n", enb_ue_s1ap_id);
    //recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    //rv = tests1ap_enb_read(sock, recvbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);

    sem_post(&occupied_s1ap_read);
    sem_wait(&received_sem_ue[enb_ue_s1ap_id-1]);
    log_update( log->initial_context_setup_receive_time, log->last_update);
    s1ap_decode_pdu(&auth_req_message,recvbuf_thread[enb_ue_s1ap_id-1]);
    //struct S1AP_InitialContextSetupRequestIEs *initialContextSetupRequestIE;
    // get item 3 
    if (auth_req_message.choice.initiatingMessage->value.choice.InitialContextSetupRequest.protocolIEs.list.array[3]->value.present != S1AP_InitialContextSetupRequestIEs__value_PR_E_RABToBeSetupListCtxtSUReq) // assumption: always item 3
            fprintf(stderr,"InitialContextSetupRequest: Item 3 is not E_RABToBeSetupListCtxtSUReq\n");
    //initialContextSetupRequestIE = auth_req_message.choice.initiatingMessage->value.choice.InitialContextSetupRequest.protocolIEs.list.array[3];
    S1AP_E_RABToBeSetupItemCtxtSUReqIEs_t *item;
    item = (S1AP_E_RABToBeSetupItemCtxtSUReqIEs_t*)auth_req_message.choice.initiatingMessage->value.choice.InitialContextSetupRequest.protocolIEs.list.array[3]->value.choice.E_RABToBeSetupListCtxtSUReq.list.array[0];
    S1AP_TransportLayerAddress_t *epc_addr = &item->value.choice.E_RABToBeSetupItemCtxtSUReq.transportLayerAddress;
    S1AP_GTP_TEID_t *gTP_TEID = &item->value.choice.E_RABToBeSetupItemCtxtSUReq.gTP_TEID;
    c_uint8_t teid = calc_from_hex((char*)gTP_TEID->buf, gTP_TEID->size);
    
    ue_teid[enb_ue_s1ap_id-1] = teid;

    S1AP_NAS_PDU_t *nAS_PDU = item->value.choice.E_RABToBeSetupItemCtxtSUReq.nAS_PDU;
    nas_message_t nas_message; 
    nas_pdu_buf = pkbuf_alloc(0, 4000);
    memcpy( nas_pdu_buf->payload, nAS_PDU->buf + sizeof(nas_security_header_t), nAS_PDU->size - sizeof(nas_security_header_t));
    nas_pdu_buf->tot_len = nas_pdu_buf->len = nAS_PDU->size - sizeof(nas_security_header_t);

    nas_emm_decode(&nas_message,nas_pdu_buf); // get nas_pdu item

    nas_message_t esm_container;
    pkbuf_t *pkbuf;
    pkbuf = pkbuf_alloc(0, 4000);
    memcpy( pkbuf->payload, nas_message.emm.attach_accept.esm_message_container.buffer, nas_message.emm.attach_accept.esm_message_container.length); 
    pkbuf->tot_len = pkbuf->len = nas_message.emm.attach_accept.esm_message_container.length;
    
    nas_esm_decode(&esm_container,pkbuf);
	
    c_uint32_t UE_DEVICE_ADDR = esm_container.esm.activate_default_eps_bearer_context_request.pdn_address.both.addr;

    char UE_DEVICE_ADDR_STR[16];
    int len;
    len = 0 ;
    for(i=0;i<4;++i) {
	    len += sprintf(UE_DEVICE_ADDR_STR+len,"%d",UE_DEVICE_ADDR&0xff);
	    if (i!=3) len += sprintf(UE_DEVICE_ADDR_STR+len,".");
	    UE_DEVICE_ADDR >>= 8;
    }
    //printf("[UE ADDR] %s\n",UE_DEVICE_ADDR_STR) ;

    pkbuf_free(nas_pdu_buf);
    pkbuf_free(pkbuf);
    /* 
     * We cannot check it since SGW S1U ADDR is changed
     * from configuration file
     */ 
#if 0
    ABTS_TRUE(tc, memcmp(recvbuf->payload, 
        CORE_HEX(_initial_context_setup_request, 
            strlen(_initial_context_setup_request), tmp),
        recvbuf->len) == 0);
#endif
    s1ap_free_pdu(&auth_req_message);
    pkbuf_free(recvbuf_thread[enb_ue_s1ap_id-1]);

    /* Send UE Capability Info Indication */
    //printf("[SEND UE CAPABILITY INFO INDICATION] %ld\n", enb_ue_s1ap_id);
    //rv = tests1ap_build_ue_capability_info_indication(&sendbuf, msgindex); 
    pthread_mutex_lock(&s1ap_send_lock);
    log_reset_time(log->last_update);
    rv = s1ap_build_ue_capability_info_indication(&sendbuf, mme_ue_s1ap_id, enb_ue_s1ap_id );  // siqi
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    pthread_mutex_unlock(&s1ap_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    log_update( log->UE_info_time, log->last_update);

    //core_sleep(time_from_msec(300));

    /* Send Initial Context Setup Response */
    //printf("[SEND INITIAL CONTEXT SETUP RESPONSE] %ld\n", enb_ue_s1ap_id);
    //printf("[TEID]: %d\n",teid);
    pthread_mutex_lock(&s1ap_send_lock);
    log_reset_time( log->last_update);
    rv = tests1ap_build_initial_context_setup_response2(&sendbuf, mme_ue_s1ap_id, enb_ue_s1ap_id, 5, teid, ip_enb);
    //rv = s1ap_build_initial_context_setup_response(&sendbuf, mme_ue_s1ap_id, 1+ue_id); // siqi
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    pthread_mutex_unlock(&s1ap_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    log_update( log->initial_context_setup_send_time, log->last_update);

    /* Send Attach Complete + Activate default EPS bearer cotext accept */
    //printf("[SEND ATTACH COMPLETE + ACTIVATE DEFAULT EPS BEARER] %ld\n", enb_ue_s1ap_id);
    // debuging here
    //rv = tests1ap_build_attach_complete(&sendbuf, msgindex);
    pthread_mutex_lock(&s1ap_send_lock);
    log_reset_time(log->last_update);
    rv = s1ap_build_attach_complete(&sendbuf, mme_ue_s1ap_id, enb_ue_s1ap_id, knas_int); // siqi
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    pthread_mutex_unlock(&s1ap_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    log_update( log->activate_EPS_time, log->last_update);

    /* Receive EMM information */
    printf("[RECEIVE EMM INFORMATION] %ld\n", enb_ue_s1ap_id);
    //recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    //rv = tests1ap_enb_read(sock, recvbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    //CORE_HEX(_emm_information, strlen(_emm_information), tmp);
    //ABTS_TRUE(tc, memcmp(recvbuf->payload, tmp, 28) == 0);
    //ABTS_TRUE(tc, memcmp(recvbuf->payload+32, tmp+32, 20) == 0);
    sem_post(&occupied_s1ap_read);
    sem_wait(&received_sem_ue[enb_ue_s1ap_id-1]);
   
    log_update( log->receive_EMM_time, log->last_update); 

    pkbuf_free(recvbuf_thread[enb_ue_s1ap_id-1]);

    UE_FILE = fopen(UE_FILE_NAME, "w");
    fprintf(UE_FILE, "done");
    fclose(UE_FILE);

#if TEST_INITIAL_CONTEXT_SETUP_FAILURE
    /* Send Initial Context Setup Failure */
    //printf("[SEND INITIAL CONTEXT SETUP FAILURE]\n");
    pthread_mutex_lock(&s1ap_send_lock);
    log_reset_time(log->last_update);
    rv = tests1ap_build_initial_context_setup_failure(&sendbuf, msgindex);

    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    pthread_mutex_unlock(&s1ap_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
#endif
    //core_sleep(time_from_msec(300));
#if 0 /* IPv6 Stateless Auto Address Configuration */
    rv = testgtpu_build_slacc_rs(&sendbuf, 0);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = testgtpu_enb_send2(sendbuf,"192.168.5.194");
    //rv = testgtpu_enb_send(sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

printf("[GTP READ]\n");
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = testgtpu_enb_read(gtpu, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);
#endif

    /* Send GTP-U ICMP Packet */
    //printf("[SEND GTP-U ICMP PACKET]\n");
    pthread_mutex_lock(&gtpu_send_lock);
    log_reset_time(log->last_update);
    rv = testgtpu_build_ping2(&sendbuf, UE_DEVICE_ADDR_STR, "45.45.0.1", teid);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = testgtpu_enb_send2(sendbuf, ip_epc, ip_local1);
    pthread_mutex_unlock(&gtpu_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    /* Receive GTP-U ICMP Packet */
    //printf("[RECEIVE GTP-U ICMP PACKET]\n");
    sem_post(&occupied_gtpu_read);
    sem_wait(&received_sem_ue[enb_ue_s1ap_id-1]);
    pkbuf_free(recvbuf_thread[enb_ue_s1ap_id-1]);
    log_update( log->ping_time, log->last_update);

    /* Send Detach Request */
    //printf("[SEND DETACH REQUESET] %ld\n", enb_ue_s1ap_id);
    pthread_mutex_lock(&s1ap_send_lock);
    log_reset_time(log->last_update);
    rv = s1ap_build_detach_request(&sendbuf, enb_ue_s1ap_id, 0x00f110, 2/*mme_group_id*/, 1/*mme_code*/, knas_int);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    pthread_mutex_unlock(&s1ap_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    log_update( log->detach_request_time, log->last_update);

    /* Receive UE Release Command */
    //printf("[RECEIVE UE RELEASE COMMAND] %ld\n", enb_ue_s1ap_id);
    //recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    //rv = tests1ap_enb_read(sock, recvbuf);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
    sem_post(&occupied_s1ap_read);
    sem_wait(&received_sem_ue[enb_ue_s1ap_id-1]);

    pkbuf_free(recvbuf_thread[enb_ue_s1ap_id-1]);
    log_update( log->UE_release_command_time, log->last_update);

    /* Send UE Release Complete */
    pthread_mutex_lock(&s1ap_send_lock);
    log_reset_time(log->last_update);
    rv = s1ap_build_ue_context_release_complete(&sendbuf, mme_ue_s1ap_id, enb_ue_s1ap_id );

    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(enb_sock1, sendbuf);
    pthread_mutex_unlock(&s1ap_send_lock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    log_update( log->UE_release_complete_time, log->last_update);
    //printf("%ld\n",CLOCKS_PER_SEC);

    goto out;

    //core_sleep(time_from_msec(300));


#if 0 /* IPv6 is needed */
#if LINUX == 1
    rv = testgtpu_build_ping(&sendbuf, "cafe::2", "cafe::1");
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = testgtpu_enb_send2(sendbuf,"192.168.5.194");
    //rv = testgtpu_enb_send(sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* Receive GTP-U ICMP Packet */
    printf("[RECEIVE GTP-U ICMP PACKET2]\n");
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = testgtpu_enb_read(gtpu, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);
#endif
#endif
    d_assert(true, goto out,);
#if 0
    /* Retreive M-TMSI */
    printf("[RECEIVE M-TMSI]\n");
    enb_ue = enb_ue_find_by_mme_ue_s1ap_id(mme_ue_s1ap_id);
    d_assert(enb_ue, goto out,);
    mme_ue = enb_ue->mme_ue;
    d_assert(mme_ue, goto out,);
    m_tmsi = mme_ue->guti.m_tmsi;
    /*****************************************************************
     * Attach Request : Known GUTI, Integrity Protected, MAC Matched
     * Send Initial-UE Message + Attach Request + PDN Connectivity  */
    core_sleep(time_from_msec(300));

    rv = tests1ap_build_initial_ue_msg(&sendbuf, msgindex+1);
    /* Update M-TMSI */
    m_tmsi = htonl(m_tmsi);
    memcpy(sendbuf->payload + 36, &m_tmsi, 4);
    /* Update NAS MAC */
    void snow_3g_f9(c_uint8_t* key, c_uint32_t count, c_uint32_t fresh,
            c_uint32_t dir, c_uint8_t *data, c_uint64_t length, c_uint8_t *out);
    snow_3g_f9(mme_ue->knas_int, 7, 0, 0,
            sendbuf->payload + 24, (109 << 3), sendbuf->payload+20);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* Receive ESM Information Request */
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(sock, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);

#if 1 /* IMPLICIT_S1_RELEASE */
    /* Send UE Context Release Request */
    rv = tests1ap_build_ue_context_release_request(&sendbuf, msgindex);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* Receive Error Indicaation */
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(sock, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);

#else /* S1_HOLDING_TIMER */
    /* Send UE Context Release Request */
    rv = tests1ap_build_ue_context_release_request(&sendbuf, msgindex);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* Receive UE Context Release Command */
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(sock, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);

    /* Send UE Context Release Complete */
    rv = tests1ap_build_ue_context_release_complete(&sendbuf, msgindex+3);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    core_sleep(time_from_msec(300));
#endif

    /* Send ESM Information Response */
    rv = tests1ap_build_esm_information_response(&sendbuf, msgindex+1);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* Receive Initial Context Setup Request + 
     * Attach Accept + 
     * Activate Default Bearer Context Request */
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(sock, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);

    /* Send Attach Complete + 
     * Activate Default EPS Bearer Context Accept */
    rv = tests1ap_build_attach_complete(&sendbuf, msgindex+1);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* Receive EMM information */
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(sock, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);

    core_sleep(time_from_msec(300));

    /* Send Detach Request */
    //printf("[SEND DETACH REQUESET]\n");
    rv = tests1ap_build_detach_request(&sendbuf, msgindex);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* Receive UE Context Release Command */
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(sock, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);

    /* Send UE Context Release Complete */
    rv = tests1ap_build_ue_context_release_complete(&sendbuf, msgindex);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /*****************************************************************
     * Attach Request : Unknown GUTI, Integrity Protected
     * Send Initial-UE Message + Attach Request + PDN Connectivity  */
    core_sleep(time_from_msec(300));

    rv = tests1ap_build_initial_ue_msg(&sendbuf, msgindex+2);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* Receive Identity Request */
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(sock, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);

    /********** Remove Subscriber in Database */
/*
    doc = BCON_NEW("imsi", BCON_UTF8(json2));
    ABTS_PTR_NOTNULL(tc, doc);
    ABTS_TRUE(tc, mongoc_collection_remove(collection, 
            MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, &error)) 
    bson_destroy(doc);

    mongoc_collection_destroy(collection);
*/
    /* Send Identity Response */
    rv = tests1ap_build_identity_response(&sendbuf, msgindex+2);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* Receive Attach Reject */
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(sock, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);

    /* Receive UE Release Command */
    recvbuf = pkbuf_alloc(0, MAX_SDU_LEN);
    rv = tests1ap_enb_read(sock, recvbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    pkbuf_free(recvbuf);

    /* Send UE Release Complete */
    rv = tests1ap_build_ue_context_release_complete(&sendbuf, msgindex+2);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);
    rv = tests1ap_enb_send(sock, sendbuf);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    core_sleep(time_from_msec(300));

    /* eNB disonncect from MME */
    rv = tests1ap_enb_close(sock);
    ABTS_INT_EQUAL(tc, CORE_OK, rv);

    /* eNB disonncect from SGW */
    //rv = testgtpu_enb_close(gtpu);
    //ABTS_INT_EQUAL(tc, CORE_OK, rv);
#endif
    return;

out:
    /********** Remove Subscriber in Database */
/*
    doc = BCON_NEW("imsi", BCON_UTF8(json2));
    ABTS_PTR_NOTNULL(tc, doc);
    ABTS_TRUE(tc, mongoc_collection_remove(collection, 
            MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, &error)) 
    bson_destroy(doc);

    mongoc_collection_destroy(collection);
*/
    //core_sleep(time_from_msec(300));
    return ;
}

abts_suite *global_suite;
void* test_runner(void* arg) {
	int i = *(int*)arg;
    	//signal(SIGINT, hsignal);
	//printf("[i] %d\n",i);
	abts_run_test(global_suite, attach_test1, &i);
	return NULL;
}

abts_suite *test_attach(abts_suite *suite)
{
    global_suite = suite;
    int ue_id = 0;
    int i;
    int a[THREADNUM] ;
    pthread_t enb_reader, gtpu_reader, ue[THREADNUM];
    sock_init("192.168.1.2", "192.168.1.9");

    pthread_create(&enb_reader, NULL, thread_s1ap_read, NULL);
    pthread_create(&gtpu_reader, NULL, thread_gtpu_read, NULL);

    for ( i = 0 ; i < THREADNUM ; ++i ) a[i] = i;

    for ( i = 0 ; i < THREADNUM; ++i ) {
	    printf("[i] %d\n",i);
	    usleep(500 * 1000);
	    pthread_create(&ue[i], NULL, test_runner, (void*)&a[i]);
	    if ( !CONCURRENT ) pthread_join(ue[i], NULL);
    }
    for ( i = 0 ; i < THREADNUM ; ++i ) 
	    pthread_join(ue[i], NULL);
    all_terminated = 1;
    sem_post(&occupied_s1ap_read);
    sem_post(&occupied_gtpu_read);
    pthread_join(enb_reader, NULL);
    pthread_join(gtpu_reader, NULL);

    sock_close();

    FILE *output_log = fopen("output.log", "a+");

    for ( i = 0 ; i < THREADNUM ; ++i ) {
	    fprintf(output_log, "ue_id: %d\n",i);
	    fprintf(output_log, "init ue msg: %ld\n",ue_log[i].init_ue_msg_time );
	    fprintf(output_log, "auth request: %ld\n",ue_log[i].auth_request_time);
	    fprintf(output_log, "auth response: %ld\n",ue_log[i].auth_response_time);
	    fprintf(output_log, "security mode command: %ld\n",ue_log[i].security_mode_command_time );
	    fprintf(output_log, "security mode complete: %ld\n",ue_log[i].security_mode_complete_time );
	    fprintf(output_log, "ESM info request: %ld\n",ue_log[i].ESM_info_request_time );
	    fprintf(output_log, "ESM info response: %ld\n",ue_log[i].ESM_info_response_time );
	    fprintf(output_log, "UE info: %ld\n",ue_log[i].UE_info_time);
	    fprintf(output_log, "initial_context send: %ld\n",ue_log[i].initial_context_setup_send_time );
	    fprintf(output_log, "initial_context receive: %ld\n",ue_log[i].initial_context_setup_receive_time );
	    fprintf(output_log, "activate EPS: %ld\n",ue_log[i].activate_EPS_time );
	    fprintf(output_log, "EMM receive: %ld\n",ue_log[i].receive_EMM_time);

	    fprintf(output_log, "detach request: %ld\n",ue_log[i].detach_request_time);
	    fprintf(output_log, "UE release command: %ld\n",ue_log[i].UE_release_command_time );
	    fprintf(output_log, "UE release complete: %ld\n",ue_log[i].UE_release_complete_time );
	    fprintf(output_log, "\n");
    }
    fclose( output_log);
    //abts_run_test(suite, attach_test2, NULL);
    //abts_run_test(suite, attach_test3, NULL);
    //abts_run_test(suite, attach_test4, NULL);
    //abts_run_test(suite, attach_test5, NULL);

    return suite;
}
