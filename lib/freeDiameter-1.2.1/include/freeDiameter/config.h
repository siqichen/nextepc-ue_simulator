/* include/freeDiameter/config.h.  Generated from config.h.in by configure.  */
/* include/freeDiameter/config.h.in.  Generated from configure.ac by autoheader.  */


#ifndef FD_IS_CONFIG
#define FD_IS_CONFIG

#define _GNU_SOURCE  1

#ifdef __cplusplus
extern "C" {
#endif



/* Define if building universal (internal helper macro) */
/* #undef AC_APPLE_UNIVERSAL_BUILD */

/* Default Configuration Path */
#define DEFAULT_CONF_PATH "/home/internetlab/git/nextepc-ue_simulator/install/etc/nextepc/freeDiameter"

/* Default Extensions Path */
#define DEFAULT_EXTENSIONS_PATH "/home/internetlab/git/nextepc-ue_simulator/install/lib/nextepc/freeDiameter"

/* Disable SCTP */
/* #undef DISABLE_SCTP */

/* Project Binary */
#define FD_PROJECT_BINARY "freeDiameterd"

/* Project Name */
#define FD_PROJECT_NAME "freeDiameter"

/* API version of this package */
#define FD_PROJECT_VERSION_API 6

/* Major version of this package */
#define FD_PROJECT_VERSION_MAJOR 1

/* Minor version of this package */
#define FD_PROJECT_VERSION_MINOR 2

/* Patch version of this package */
#define FD_PROJECT_VERSION_REV 1

/* Define to 1 if you have gnutls 2.10 installed */
#define GNUTLS_VERSION_210 1

/* Define to 1 if you have gnutls 2.10 installed */
#define GNUTLS_VERSION_212 1

/* Define to 1 if you have gnutls 3.0 installed */
#define GNUTLS_VERSION_300 1

/* Define to 1 if you have gnutls 3.1 installed */
#define GNUTLS_VERSION_310 1

/* Define AI_ADDRCONFIG */
#define HAVE_AI_ADDRCONFIG 1

/* Define clock_gettime */
#define HAVE_CLOCK_GETTIME 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have ntohll. */
/* #undef HAVE_NTOHLL */

/* Define to 1 if you have pthread_barrier_wait in libpthread */
#define HAVE_PTHREAD_BAR 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the `strndup' function. */
#define HAVE_STRNDUP 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* Name of package */
#define PACKAGE "nextepc"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "acetcom@gmail.com"

/* Define to the full name of this package. */
#define PACKAGE_NAME "nextepc"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "nextepc 1.2.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "nextepc"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.2.1"

/* Define 1 if sctp_connectx function accepts 4 arguments */
#define SCTP_CONNECTX_4_ARGS 1

/* Disable SCTP */
/* #undef SCTP_USE_MAPPED_ADDRESSES */

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "1.2.1"

/* Define WORDS_BIGENDIAN to 1 if your processor stores words with the most
   significant byte first (like Motorola and SPARC, unlike Intel). */
#if defined AC_APPLE_UNIVERSAL_BUILD
# if defined __BIG_ENDIAN__
#  define WORDS_BIGENDIAN 1
# endif
#else
# ifndef WORDS_BIGENDIAN
/* #  undef WORDS_BIGENDIAN */
# endif
#endif

/* Define to 1 if `lex' declares `yytext' as a `char *' by default, not a
   `char[]'. */
#define YYTEXT_POINTER 1


#define FD_PROJECT_COPYRIGHT "Copyright (c) 2008-2015, WIDE Project (www.wide.ad.jp) and NICT (www.nict.go.jp)"

#ifndef FD_DEFAULT_CONF_FILENAME
#define FD_DEFAULT_CONF_FILENAME "freeDiameter.conf"
#endif /* FD_DEFAULT_CONF_FILENAME */

/* Maximum number of hooks handlers that can be registered. Make this compilation option if needed */
#define FD_HOOK_HANDLE_LIMIT	5

#ifdef __cplusplus
}
#endif

#endif /* FD_IS_CONFIG */

